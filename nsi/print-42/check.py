from pathlib import Path

import correction_helper as checker

checker.exclude_file_from_traceback(__file__)
Path("solution").rename("solution.py")


def check():
    output = checker.run("solution.py")
    if output == "42":
        print("""42 est la bonne réponse, bien joué !""")
        exit(0)
    solution = Path("solution.py").read_text()
    if any(quote in solution for quote in ('"', "'")):
        checker.fail(
            "Si vous utilisez des guillemets, ce n'est pas un nombre, "
            "mais une chaîne de caractères",
            "Votre code affiche :",
            checker.code(output),
        )
    if "print" not in solution and "42" in solution:
        checker.fail(
            """Vous n'êtes pas dans un REPL Python ici,
il n'y a donc pas de [print](https://docs.python.org/3/library/functions.html#print)
implicite, vous devez appeler la fonction `print()`."""
        )
    if not output:
        checker.fail(
            """Votre code n'affiche rien, avez-vous oublié d'appeler la fonction
[print](https://docs.python.org/3/library/functions.html#print) ?"""
        )
    if "42" in output:
        checker.fail(
            "Presque ! J'ai juste besoin d'un `42`, rien de plus, rien de moins.",
            "Votre code affiche :",
            checker.code(output),
        )
    checker.fail(
        """Ça n'est pas bon. J'ai juste besoin que votre code
[affiche](https://docs.python.org/3/library/functions.html#print) `42`.""",
        "Votre code m'a affiché :",
        checker.code(output),
    )


if __name__ == "__main__":
    check()
