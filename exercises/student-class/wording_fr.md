Implémentez les classes `Student`, `School`, et `City` tel que :

 - `Student`, `School`, et `City` ont un attribut `name` donné au constructeur.
 - La classe `Student` a une méthode `add_exam(mark)`, pour ajouter des notes aux étudiants.
 - La classe `School` a une méthode `add_student(student)`.
 - La classe `City` a une méthode `add_school(school)`.
 - `Student`, `School`, et `City` ont une méthode `get_mean()` donnant :
   - Pour un étudiant, la moyenne de ses notes.
   - Pour une école, la moyenne des moyennes de ses étudiants.
   - Pour une ville, la moyenne des moyennes des écoles.
 - Les écoles ont une méthode `get_best_student()`, renvoyant le meilleur étudiant.
 - Les villes ont les méthodes `get_best_school()` et `get_best_student()` renvoyant
   respectivement une école et un étudiant.

## Exemple

```python
paris = City('paris')
hkis = School('hkis')
paris.add_school(hkis)
for student_name, student_grades in (('alice', (1, 2, 3)),
                                    ('bob', (2, 3, 4)),
                                    ('catherine', (3, 4, 5)),
                                    ('daniel', (4, 5, 6))):
    student = Student(student_name)
    for grade in student_grades:
        student.add_exam(grade)
    hkis.add_student(student)
print(paris.get_best_school().name)
print(paris.get_best_student().name)
```

## Références

- Classes: <https://docs.python.org/fr/3/tutorial/classes.html>
