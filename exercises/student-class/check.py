from pathlib import Path

import correction_helper as checker

checker.exclude_file_from_traceback(__file__)
Path("solution").rename("solution.py")

with checker.student_code():
    from solution import City, School, Student


SCHOOL = {
    "alice": (1, 2, 3),
    "bob": (2, 3, 4),
    "catherine": (3, 4, 5),
    "daniel": (4, 5, 6),
}


def check():
    with checker.student_code(prefix="While creating `Student` objects"):
        ada = Student("ada")
        alan = Student("alan")
    with checker.student_code(prefix="While adding exams"):
        ada.add_exam(1.1)
        ada.add_exam(2.2)
        alan.add_exam(3.3)
        alan.add_exam(4.4)
    with checker.student_code(prefix="While calling your Student's `get_mean` method"):
        equals = ada.get_mean() == alan.get_mean()
    if ada.get_mean() is None:
        checker.fail(
            "If I create just one students (no school, no city), "
            "assign it a few marks, an call `get_mean()`, I'm getting `None`.",
            "I expected a value.",
        )
    if equals:
        checker.fail(
            """If I create just two students (no school, no city),
assign them different marks (like 1 for the first, 2 for the other),
they end up with the same mean, which is wrong.

Maybe you stored marks as a class attribute instead of an instance attribute?

Or used a mutable default argument?""",
        )
    with checker.student_code():
        paris = City("paris")
        hkis = School("hkis")
        paris.add_school(hkis)
    if not hasattr(paris, "name"):
        checker.fail("""You city should have a `name` attribute.""")
    if paris.name != "paris":
        checker.fail(
            """I created `paris = City("paris")`, and checked for `city.name`,
I expected to get `'paris'` back, but you gave `{city_name!r}`""".format(
                city_name=paris.name
            )
        )

    for student_name, student_marks in SCHOOL.items():
        with checker.student_code():
            student = Student(student_name)
        for mark in student_marks:
            with checker.student_code():
                student.add_exam(mark)
        with checker.student_code():
            hkis.add_student(student)

    with checker.student_code(prefix="While calling your Scools's `get_mean` method"):
        hkis_mean = hkis.get_mean()
    if not isinstance(hkis_mean, float):
        checker.fail(f"Your school's get_mean don't return a float, got: {hkis_mean!r}")

    with checker.student_code(prefix="While calling your City's `get_mean` method"):
        paris_mean = paris.get_mean()

    if not isinstance(paris_mean, float):
        checker.fail(f"Your city's get_mean don't return a float, got: {paris_mean!r}")

    with checker.student_code():
        student = hkis.get_best_student()

    if not isinstance(student, Student):
        checker.fail(
            "`get_best_student` from a `School` instance should return a `Student`."
            f"Got a: {student!r}"
        )

    with checker.student_code():
        best_school = paris.get_best_school()
    if best_school is None:
        checker.fail(
            """You `City.get_best_school` function is returning `None`,
expected a `School` instance."""
        )
    if not isinstance(best_school, School):
        checker.fail(
            "`get_best_school` is expected to return a `School` instance, "
            f"your implementation returned a `{type(best_school)}`"
        )
    with checker.student_code():
        paris_school_name = best_school.name
    if paris_school_name != "hkis":
        checker.fail(
            "In my first test with a single school, you're not telling me my"
            "single school is the best, it obviously is. You're giving me:",
            checker.code(paris_school_name),
        )
    with checker.student_code():
        best_student = paris.get_best_student()
    if not isinstance(best_student, Student):
        checker.fail(
            f"""Your City's `get_best_student` is returning a `{type(best_student)}`
instead of a `Student`."""
        )
    with checker.student_code():
        best_student_name = best_student.name
    if best_student_name != "daniel":
        checker.fail(
            "In a city with a single school with 4 students like:",
            checker.code(repr(SCHOOL), "python"),
            "If I ask you for the best student of the city, "
            "you're not telling me the best student is daniel with 4,5,6, "
            "you're giving me:",
            checker.code(repr(best_student), "python"),
            f"whose name is `{repr(best_student_name)}`.",
        )


if __name__ == "__main__":
    check()
