from pathlib import Path

import correction_helper as checker
from sympy.ntheory import factorint

checker.exclude_file_from_traceback(__file__)
Path("solution").rename("solution.py")


def factors(n):
    f = factorint(n, multiple=True)
    if n in f:  # I don't care to learn it can be divided by itself.
        f.remove(n)
    return f


def check():
    output = checker.run("solution.py")
    if not output:
        checker.fail("Your code printed nothing.")
    if output.endswith(","):
        checker.fail(
            "Expected your values to be separated by a coma and a space, "
            "but I don't want a trailing coma.",
            "Your full output is:",
            checker.code(output),
        )
    values = output.split(", ")
    for value in values:
        try:
            intvalue = int(value)
        except ValueError:
            checker.fail(
                f"`{value}` is not an integer. Expected your values to be separated "
                "by a coma and a space, your full output is:",
                checker.code(output),
            )
        if intvalue < 10_000 or intvalue > 10_050:
            checker.fail(
                f"`{intvalue}` is not in the range [10000;10050]!",
                "Your full output is:",
                checker.code(output),
            )
        for factor in factors(intvalue):
            checker.fail(
                f"`{intvalue}` is divisible by {factor}, so it's not a prime!",
                "I need your code to only print prime numbers.",
                "Your full output is:",
                checker.code(output),
            )


if __name__ == "__main__":
    check()
