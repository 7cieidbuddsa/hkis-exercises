from gettext import gettext, textdomain
import subprocess
from math import isclose
from pathlib import Path

import correction_helper as checker

checker.exclude_file_from_traceback(__file__)
Path("solution").rename("solution.py")
textdomain("hkis")

USAGE = (
    "a_number (an_operator +-*/%^) a_number",
    "usage: solution.py [-h]",
)


def run(file, *args):
    if args:
        args = ["--"] + list(args)
    proc = subprocess.run(
        [
            "python3",
            "-m",
            "friendly_traceback",
            "--formatter",
            "correction_helper.friendly_traceback_markdown",
            file,
            *args,
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        universal_newlines=True,
    )
    return proc.stdout.strip()


def test_program(argv, expect):
    got = run("solution.py", *argv)
    if expect is None:
        if got != "input error":
            checker.fail(
                f"Got an unexpected response for `{' '.join(argv)}`, "
                "expected `input error`.",
                "Got:",
                checker.code(got) if "## " not in got else got,
            )
        return
    if expect is False:
        if all(usage not in got for usage in USAGE):
            checker.fail(
                gettext(
                    "I called your program with: `{argv}` "
                    "so I expected a usage line, but got:"
                ).format(argv=" ".join(argv)),
                checker.code(got) if "## " not in got else got,
            )
        return
    if "## " in got:  # Traceback from friendly
        checker.fail(
            f"Got an unexpected response for `{' '.join(argv)}`:",
            got,
        )
    try:
        got = float(got)
    except Exception as err:
        checker.fail(
            f"Cannot parse your result as a number (got {err}).",
            "Given:",
            checker.code(repr(argv)),
            "your program printed:",
            checker.code(got),
        )
    if not isclose(got, expect):
        checker.fail(
            f"Got an unexpected response for `{' '.join(argv)}`, "
            f"expected `{expect!r}`.",
            "Got:",
            checker.code(got),
        )


def check():
    output = run("solution.py")
    if all(usage not in output for usage in USAGE):
        if output == "":
            checker.fail(
                gettext(
                    "With no parameter, I expected your program "
                    "to print the usage line, you printed nothing."
                ),
                gettext(
                    "(Did you forgot to call your function? I'm asking for a program, "
                    "not a function in this exercise)."
                ),
            )
        checker.fail(
            gettext(
                "With no parameter, I expected your program to print the usage "
                "line, but I got:"
            ),
            output if "## " in output else checker.code(output),
        )
    test_program(["1"], False)
    test_program(["1", "2"], False)
    test_program(["5", "+", "5"], 5 + 5)
    test_program(["5", "-", "5"], 0)
    test_program(["5", "*", "5"], 5 * 5)
    test_program(["5", "%", "5"], 5 % 5)
    got = run("solution.py", "2", "^", "32")
    if got == "34":
        checker.fail(
            "Error: got `34` for `2 ^ 32`.",
            "Beware the `^` operator is not the power operator (but `xor`).",
            "The power operator in Python is `**`",
        )

    test_program(["2", "^", "32"], 2**32)
    test_program(["1", "1", "1"], None)
    test_program(["a", "1", "1"], None)
    test_program(["5", "/", "0"], None)


if __name__ == "__main__":
    check()
