from pathlib import Path

import correction_helper as checker

checker.exclude_file_from_traceback(__file__)
Path("solution").rename("solution.py")


def main():
    out = checker.run("solution.py")
    if out != "Not enough parameters.":
        checker.fail(
            "Your error message seems wrong, I tried running your "
            "program without any parameter, expected:",
            checker.code("Not enough parameters."),
            "Got:",
            checker.code(out),
        )
    if "if " in Path("solution.py").read_text():
        checker.fail(
            """Cheater, you tried to use an `if`?

As explicitly mentionned, any use of an `if` in this exercise is strictly prohibited.

Please stick to a simple 4 line answer with a try/except block.
"""
        )
    got = checker.run("solution.py", "12")
    if got != "12":
        checker.fail(
            f"""You should print the first argument when given, I called your
script with `12` as a parameter.

You printed:

{checker.code(got)}
"""
        )
    got = checker.run("solution.py", "12", "13")
    if got != "12":
        checker.fail("You should ONLY print the first argument.")


if __name__ == "__main__":
    main()
