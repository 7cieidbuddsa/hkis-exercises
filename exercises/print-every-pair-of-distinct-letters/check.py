import string
from itertools import combinations
from pathlib import Path

import correction_helper as checker

checker.exclude_file_from_traceback(__file__)
Path("solution").rename("solution.py")


def check():
    output = checker.run("solution.py")
    if output == "":
        checker.fail(
            "You printed nothing, did you forgot to call the [print]("
            "https://docs.python.org/3/library/functions.html#print) function?"
        )
    combs = "\n".join(a + b for a, b in combinations(string.ascii_lowercase, 2))
    if output == combs:
        checker.fail(
            "I'm not asking for combinations of two letters, "
            "but every two letter pairs. I mean I want `ba` AND `ab`."
        )
    if "ab" not in output and "a b" in output:
        checker.fail(
            "You printed 'a b' instead of 'ab'.", "You printed:", checker.code(output)
        )
    for pair in "ab", "ac", "ad", "ba", "bc", "xy", "xz":
        if pair not in output:
            checker.fail(
                f"Why not printing `{pair}`?", "You printed:", checker.code(output)
            )
    for dup in "aa", "bb", "mm", "zz", "yy":
        if dup in output:
            checker.fail(
                f"You printed the duplicate: `{dup}`, you should not.",
                "You printed:",
                checker.code(output),
            )
    if len(output.split("\n")) < (26 * 26 - 26):
        checker.fail(
            "Your output seems a bit long.", "You printed:", checker.code(output)
        )
    if len(output.split("\n")) > (26 * 26 - 26):
        checker.fail(
            "Your output seems a bit short.", "You printed:", checker.code(output)
        )


if __name__ == "__main__":
    check()
